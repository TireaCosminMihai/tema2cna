﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Grpc.Net.Client;

namespace ZodiacClient
{
    class Program
    {
        public static bool Onlynumbers(String date)
        {
            bool onlynumbers = true;
            for (int index = 0; index < date.Length; index++)
                if (!(Char.IsDigit(date[index])) && date[index] != '/')
                    onlynumbers = false;
            return onlynumbers;
        }

        public static bool Leapyear(int year)
        {
            bool leapyear = false;
            if (year % 4 == 0 && year % 100 != 0)
                leapyear = true;
            if (year % 400 == 0)
                leapyear = true;
            return leapyear;
        }

        static async Task Main()
        {
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            Console.WriteLine("Welcome to the Zodiac Sign finder by date of birth!");
            Console.WriteLine("Please enter your date of birth by format mm/dd/yyyy (e.g. 01/01/1999) : ");
            string date = Console.ReadLine();
            int dayToCheck=0 ;
            int monthToCheck=0 ;
            int yearToCheck=0 ;
            while (Onlynumbers(date) == false || date.Length != 10 || date[2]!='/' || date[5]!='/')
            {
                Console.WriteLine("Input error. Please enter the date of birth again (mm/dd/yyyy) ");
                date = Console.ReadLine();
            }

            int inputverified = 0;
            while (inputverified != 1)
            {
                while (Onlynumbers(date) == false || date.Length != 10 || date[2] != '/' || date[5] != '/')
                {
                    Console.WriteLine("Input error. Please enter the date of birth again (mm/dd/yyyy) ");
                    date = Console.ReadLine();
                }
                inputverified = 1;
                string day, month, year;
                if (date[0] == '0')
                    month = date.Substring(1, 1);
                else
                    month = date.Substring(0, 2);
                //Console.WriteLine(month);
                if (date[3] == '0')
                    day = date.Substring(4, 1);
                else
                    day = date.Substring(3, 2);
                //Console.WriteLine(day);
                year = date.Substring(6, 4);
               // Console.WriteLine(year);
                dayToCheck = Convert.ToInt32(day);
                monthToCheck = Convert.ToInt32(month);
                yearToCheck = Convert.ToInt32(year);
                
                if (yearToCheck > 2021 || yearToCheck < 1800)
                {
                    inputverified = 0;
                }
                if (monthToCheck < 1 || monthToCheck > 12)
                {
                    inputverified = 0;
                }
                if (dayToCheck < 1 || dayToCheck > 31)
                {
                    inputverified = 0;
                }

                int isleap = 0;
                if (Leapyear(yearToCheck) == true)
                    isleap = 1;

                if (monthToCheck == 4 || monthToCheck == 6 || monthToCheck == 9 || monthToCheck == 11)
                {
                    if (dayToCheck == 31)
                    {
                        inputverified = 0;
                    }
                }

                if (monthToCheck == 2)
                {
                    if (dayToCheck == 31 || dayToCheck == 30)
                    {
                        inputverified = 0;
                    }
                    if (isleap == 0 && dayToCheck == 29)
                    {
                        inputverified = 0;
                    }
                }
                if (inputverified == 0 )
                {
                    Console.WriteLine("Input error. Please enter the date of birth again (mm/dd/yyyy) ");
                    date = Console.ReadLine();
                }
            }

            var client = new Zodiac.ZodiacClient(channel);
            var reply = await client.addDateAsync( new DateRequest { Day=dayToCheck,Month=monthToCheck});
            Console.WriteLine( reply.Message );
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}