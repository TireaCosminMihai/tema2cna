using Grpc.Core;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ZodiacService
{
    public class GreeterService : Zodiac.ZodiacBase
    {

        public override Task<ZodiacSign> addDate(DateRequest request, ServerCallContext context)
        {
            string astro_sign=null;
            if (request.Month == 12)
            {

                if (request.Day < 22)
                    astro_sign = "Sagittarius";
                else
                    astro_sign = "Capricorn";
            }

            else if (request.Month == 1)
            {
                if (request.Day < 20)
                    astro_sign = "Capricorn";
                else
                    astro_sign = "Aquarius";
            }

            else if (request.Month == 2)
            {
                if (request.Day < 19)
                    astro_sign = "Aquarius";
                else
                    astro_sign = "Pisces";
            }

            else if (request.Month == 3)
            {
                if (request.Day < 21)
                    astro_sign = "Pisces";
                else
                    astro_sign = "Aries";
            }
            else if (request.Month == 4)
            {
                if (request.Day < 20)
                    astro_sign = "Aries";
                else
                    astro_sign = "Taurus";
            }

            else if (request.Month == 5)
            {
                if (request.Day < 21)
                    astro_sign = "Taurus";
                else
                    astro_sign = "Gemini";
            }

            else if (request.Month == 6)
            {
                if (request.Day < 21)
                    astro_sign = "Gemini";
                else
                    astro_sign = "Cancer";
            }

            else if (request.Month == 7)
            {
                if (request.Day < 23)
                    astro_sign = "Cancer";
                else
                    astro_sign = "Leo";
            }

            else if (request.Month == 8)
            {
                if (request.Day < 23)
                    astro_sign = "Leo";
                else
                    astro_sign = "Virgo";
            }

            else if (request.Month == 9)
            {
                if (request.Day < 23)
                    astro_sign = "Virgo";
                else
                    astro_sign = "Libra";
            }

            else if (request.Month == 10)
            {
                if (request.Day < 23)
                    astro_sign = "Libra";
                else
                    astro_sign = "Scorpio";
            }

            else if (request.Month == 11)
            {
                if (request.Day < 22)
                    astro_sign = "Scorpio";
                else
                    astro_sign = "Sagittarius";
            }
            Console.WriteLine("The user zodiac sign which is born in day " + request.Day + " and month " + request.Month + " is " + astro_sign +" ! ");
            return Task.FromResult(new ZodiacSign
            {

                Message = "Your zodiac sign is: " + astro_sign + " ! " +"\n"+"Thank you for using this software!"

            }) ;
        }
    }
}
